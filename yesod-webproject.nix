{ mkDerivation, aeson, base, bytestring, case-insensitive
, classy-prelude, classy-prelude-conduit, classy-prelude-yesod
, conduit, containers, data-default, directory, fast-logger
, file-embed, hjsmin, hspec, http-conduit, monad-control
, monad-logger, persistent, persistent-sqlite, persistent-template
, safe, shakespeare, stdenv, template-haskell, text, time
, unordered-containers, vector, wai, wai-extra, wai-logger, warp
, yaml, yesod, yesod-core, yesod-form, yesod-persistent
, yesod-static, yesod-test
}:
mkDerivation {
  pname = "yesod-webproject";
  version = "0.0.0";
  src = ./.;
  isLibrary = true;
  isExecutable = true;
  enableSeparateDataOutput = true;
  libraryHaskellDepends = [
    aeson base bytestring case-insensitive classy-prelude
    classy-prelude-conduit classy-prelude-yesod conduit containers
    data-default directory fast-logger file-embed hjsmin http-conduit
    monad-control monad-logger persistent persistent-sqlite
    persistent-template safe shakespeare template-haskell text time
    unordered-containers vector wai wai-extra wai-logger warp yaml
    yesod yesod-core yesod-form yesod-persistent yesod-static
  ];
  executableHaskellDepends = [
    aeson base bytestring case-insensitive classy-prelude
    classy-prelude-conduit classy-prelude-yesod conduit containers
    data-default directory fast-logger file-embed hjsmin http-conduit
    monad-control monad-logger persistent persistent-sqlite
    persistent-template safe shakespeare template-haskell text time
    unordered-containers vector wai wai-extra wai-logger warp yaml
    yesod yesod-core yesod-form yesod-persistent yesod-static
  ];
  testHaskellDepends = [
    aeson base bytestring case-insensitive classy-prelude
    classy-prelude-conduit classy-prelude-yesod conduit containers
    data-default directory fast-logger file-embed hjsmin hspec
    http-conduit monad-control monad-logger persistent
    persistent-sqlite persistent-template safe shakespeare
    template-haskell text time unordered-containers vector wai
    wai-extra wai-logger warp yaml yesod yesod-core yesod-form
    yesod-persistent yesod-static yesod-test
  ];
  license = stdenv.lib.licenses.gpl3;
}
