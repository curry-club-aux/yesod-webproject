{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE QuasiQuotes #-}

module Handler.Home where

import Import
import Yesod.Form.Bootstrap3 (BootstrapFormLayout (..), renderBootstrap3)
import Text.Julius (RawJS (..))
import Yesod.Core
import Yesod.Form

data Registration = Registration
    { name :: Text
    , email :: Text
    , futter :: Food
    , schlafen :: Text
    , fileInfo :: FileInfo
    , fileDescription :: Text
    }

data Food = Vegan | Vegetarisch | Alles
    deriving (Show, Eq, Enum, Bounded)

getHomeR :: Handler Html
getHomeR = do
    (formWidget, formEnctype) <- generateFormPost $ renderBootstrap3 BootstrapBasicForm $ registrationForm Nothing
    let submission = Nothing :: Maybe Registration
        handlerName = "getHomeR" :: Text
    defaultLayout $ do
        setTitle "GRODEN 2018 - Anmeldung"
        $(widgetFile "homepage")

postHomeR :: Handler Html
postHomeR = do
    ((result, formWidget), formEnctype) <- runFormPost $ renderBootstrap3 BootstrapBasicForm $ registrationForm Nothing
    let submission = Nothing :: Maybe Registration
    case result of
            FormSuccess registration -> do
              redirect $ ResultR
            _ -> defaultLayout $(widgetFile "homepage")

registrationForm :: Maybe Registration -> AForm Handler Registration
registrationForm mRegistrationForm = Registration
    <$> areq textField nameSettings (name <$> mRegistrationForm)
    <*> areq textField mailSettings (email <$> mRegistrationForm)
    <*> areq (selectFieldList foods) futterSettings (futter <$> mRegistrationForm)
    <*> areq textField schlafenSettings (schlafen <$> mRegistrationForm)
    <*> fileAFormReq "Choose a file"
    <*> areq textField textSettings (fileDescription <$> mRegistrationForm)
    where textSettings = FieldSettings "What's on the file?" Nothing Nothing Nothing [ ("class", "form-control"), ("placeholder", "File description")]
          nameSettings = FieldSettings "Name" Nothing (Just "name") (Just "name") [ ("class", "form-control"), ("placeholder", "Lian")]
          mailSettings = FieldSettings "E-Mail" Nothing (Just "email") (Just "email") [ ("class", "form-control"), ("placeholder", "mail@example.com") ]
          futterSettings = FieldSettings "Futter" Nothing (Just "futter") (Just "futter") [ ("class", "form-control"), ("placeholder", "vegan, vegetarisch, alles")]
          schlafenSettings = FieldSettings "Schlafen" Nothing (Just "schlafen") (Just "schlafen") [ ("class", "form-control"), ("placeholder", "Eigenes Zelt, Gemeinschaftsschlafraum, Eigener Wohnwagen/Auto (nicht auf Gelände!), Ich übernachte nicht")]
          foods :: [(Text, Food)]
          foods = [("vegan", Vegan), ("vegetarisch", Vegetarisch), ("alles", Alles)]
