{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
module Handler.HomeSpec (spec) where

import TestImport

spec :: Spec
spec = withApp $ do

    describe "Homepage" $ do
      it "loads the index and checks it looks right" $ do
          get HomeR
          statusIs 200
          htmlAnyContain "h1" "GRODEN 2018 - Anmeldung"

          request $ do
              setMethod "POST"
              setUrl HomeR
              addToken
              fileByLabel "Choose a file" "test/Spec.hs" "text/plain" -- talk about self-reference
              byLabel "What's on the file?" "Some Content"
              byLabel "Name" "Lian"
              byLabel "E-Mail" "mail@example.com"
              byLabel "Futter" "vegetarisch"
              byLabel "Schlafen" "Ich übernachte nicht"

          -- more debugging printBody
          --htmlAllContain ".upload-response" "text/plain"
          --htmlAllContain ".upload-response" "Some Content"
