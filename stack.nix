with import <nixpkgs> {};

haskell.lib.buildStackProject {
  name = "yesod-webproject";
  src = ./.;
  buildInputs = [
    zlib
  ];
}

