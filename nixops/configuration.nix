# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/vda";

  # networking.hostName = "nixos"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.
  networking.hostName = "mars";
  networking.usePredictableInterfaceNames = false;
  networking.interfaces.eth0.ip4 = [ { address = "10.67.0.2"; prefixLength = 8; } ];
  networking.interfaces.eth0.ip6 = [ { address = "2a01:4f8:190:8243:a1:b67:0:2"; prefixLength = 64; } ];
  networking.defaultGateway = "10.22.0.2";
  networking.defaultGateway6 = { address = "2a01:4f8:190:8243::2"; interface = "eth0"; };
  networking.nameservers = [ "2a01:4f8:0:a102::add:9999" "2a01:4f8:0:a111::add:9898" "2a01:4f8:0:a0a1::add:1010" "213.133.100.100" "213.133.99.99" "213.133.98.98" ];


  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # };

  # Set your time zone.
  # time.timeZone = "Europe/Amsterdam";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [
    wget vim
  ];

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;

  # Enable CUPS to print documents.
  # services.printing.enable = true;

  # Enable the X11 windowing system.
  # services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";

  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  # services.xserver.displayManager.sddm.enable = true;
  # services.xserver.desktopManager.plasma5.enable = true;

  services.openssh.enable = true;
  users.users.root.openssh.authorizedKeys.keys = ["ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCkZNhO4jruCaTNqjVImNJRWV+YSoUfuS7pDft0vOiNxVgGuLFB19BsfGr82lMc5WnGMsJeAsCs1s6HnuMjeJ2q9EL4LU3Ia6x+R8OgqFK+sa4uR0+nVtaDmnYZM2dfrzYrUuO4Q4qtikPh64Pl2eKlDu9D5gFL+Kp47zSqqTqFgNGacoxQAZ+53ijHhJMsLX9Ra0kXN3gs+vpwZZm79WPi96WXJGzSoXrqQbOSEKlXujiUOCzTic7G7ySAqwECsnGNSVdDOrl5yZrgmv7FT8zo0RtHhFmlhcHPniSbBHB2507Gtj1WjH370I3VbYCfyDa+MGqMphIkKtPfbDoAx+HkcOXJSFyT1HfkiFT9fgeZKgRkXjWs54v2FsFLD/h1/F6+LFI/2DZdsjyphXvkCz0GDKUFmBlcdBdmpe3dREfnGDq6spl6RbTKmT3cs2nflsIsZKokku8rc3WE5gx9hUy9YMf7toeqo+NOEILfFLskkhv7Cwf82nq5viHZS38qprrKEFnFxiUA1KUK+zn76V2DnwVPHr58DkeFt5RAWR84WYFZuWt/Z0+Q36XPLF3nG6UV4SdgYF/+glBDAv7NgCB+heYsVlsrBn4rcd7Y4LlSl/BEV+NMPwi4EGnc4A0l2f/Wk784aYvJCnSCtqOg0EQxwK8oZBaMfaK0HTnEyTfCNw== seelmann@t430-2012-07-16" "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC6RzaA1Bx1WZc+RId1IgYLWgtBU+tSRfkqxtFdOwtkS0f2fD7j5U+2c4G3i4Cra/YIFpCVRdPMWrwLF26IQu7ivr3rk7peFlrbEoRkvxh3ByL08DrnfPhyCKOKQ34ocacq2lV0CJfhvc+625uwu4uKiOC4g8kV5bPxF2+cyjwiXO8fP27ybZ4QJfWhfpsNpRcAK3Oaup9lBuUbQGMh2J3nMkLhcieEUgFRhjeIDfuF6suE46BWk5i4rIRtg7+9L43hT49Az96EfWIBEyyxb7KKjdonud6qVs0pRmibsS2iPNo1xo+iFgBwJoYqDa+ZDEpilJ4MuzrirXhb5AHeBkAJ cko"];

  # Define a user account. Don't forget to set a password with ‘passwd’.
  # users.extraUsers.guest = {
  #   isNormalUser = true;
  #   uid = 1000;
  # };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "17.09"; # Did you read the comment?

}
