{
  network.description = "yesod-webproject";

  defaults = {
    imports = [
      ./configuration.nix
    ];
  };

  webserver =
    { configs, pkgs, ... }:
    let 
      app = import ../default.nix {};
    in
    { 
      deployment.targetEnv = "none";
      deployment.targetHost = "2a01:4f8:190:8243:a1:b67:0:2";

      networking.firewall.allowedTCPPorts = [ 22 80 ];

      services.nginx = {
        enable = true;
        httpConfig = ''
        upstream backend {
          server 127.0.0.1:3000;
        }
        server {
          listen [::]:80;
          listen 80;
          location / {
            proxy_pass http://backend;
          }
        }
        '';
      };

      systemd.services.yesod-webproject = {
        description = "yesod-webproject";
        after = [ "network.target" ];
        wantedBy = [ "multi-user.target" ];
        serviceConfig = {
          ExecStartPre = ["/run/current-system/sw/bin/mkdir -p /tmp/yesod-webproject" "/run/current-system/sw/bin/cp -a ${app.data}/share/ghc-8.0.2/x86_64-linux-ghc-8.0.2/yesod-webproject-0.0.0/config /tmp/yesod-webproject/" "/run/current-system/sw/bin/cp -a ${app.data}/share/ghc-8.0.2/x86_64-linux-ghc-8.0.2/yesod-webproject-0.0.0/static /tmp/yesod-webproject/"];
          WorkingDirectory = "-/tmp/yesod-webproject";
          ExecStart = "${app}/bin/yesod-webproject";
          Restart = "always";
        };
      };
    };
}
