with import <nixpkgs> {};
{ nixpkgs ? import <nixpkgs> {}, compiler ? "ghc802" }:
(
  let x = import ./default.nix {
    inherit nixpkgs compiler;
  };
  in nixpkgs.stdenv.mkDerivation {
    name = x.pname;
    buildInputs = [ nixops stack haskellPackages.cabal2nix ];
    import = [ ./default.nix ];
  }  
)

