{-# LANGUAGE PackageImports #-}
import "yesod-webproject" Application (develMain)
import Prelude (IO)

main :: IO ()
main = develMain
